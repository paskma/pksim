package examples.absim;

import examples.abreal.AImpl;
import examples.abreal.BImpl;
import org.junit.Test;
import pksim.core.Component;
import pksim.core.Simulation;

public class ABSimMainTest {
	private static final int A1_ID = 1, B_ID = 3;

	@Test
	public void iterations() throws InterruptedException {
		Simulation simulation = new Simulation();

		BStub bStub1 = new BStub(simulation, A1_ID, B_ID);

		AImpl aImpl1 = new AImpl(bStub1);
		Component cA1 = new Component(A1_ID, aImpl1);
		simulation.addComponent(cA1);

		BImpl bImpl = new BImpl();
		Component cB = new Component(B_ID, bImpl);
		simulation.addComponent(cB);

		simulation.loopLimit = 40;
		Thread simThread = simulation.start();
		aImpl1.loopLimit = 10;
		Thread t1 = aImpl1.start(A1_ID);

		t1.join();
		simThread.join();
	}
}
