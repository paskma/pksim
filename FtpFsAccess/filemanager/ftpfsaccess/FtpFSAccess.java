package filemanager.ftpfsaccess;

import filemanager.utils.IFSNode;
import filemanager.utils.IFileAccess;
import pyftpclient_layer.CClient;
import pyftpclient_layer.CClientFactory;
import pyftpclient_layer.CClientFactoryImpl;

/**
 * Implementation of file access via FTP protocol.
 */
public class FtpFSAccess implements IFileAccess {
	private CClientFactory clientFactory = new CClientFactoryImpl(CClient.NET_TEST);

	public void setCClientFactory(CClientFactory factory) {
		this.clientFactory = factory;
	}

	@Override
	public IFSNode getFSNode(String pathname) {
		return new FtpFSNode(pathname, clientFactory.createCClient());
	}

	@Override
	public boolean deleteFSNode(String pathname, boolean deleteSubdir) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean copyFSNode(String sourceName, String destName) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean renameFSNode(String oldName, String newName) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
