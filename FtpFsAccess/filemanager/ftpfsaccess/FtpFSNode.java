package filemanager.ftpfsaccess;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import examples.componentftpreal.IRecreatingCClient;
import examples.componentftpsim.RecreatingCClient;
import pyftpclient_layer.CFileStream;
import pyftpclient_layer.CFtpFile;
import filemanager.utils.IFSNode;
import pyftpclient_layer.ICClient;

/**
 * This implementation is currently a MOCKUP!
 */
public class FtpFSNode implements IFSNode {
	private String name;
	private String pathname;
	private boolean directory;
	private final FtpFSNode parentNode;
	/** exists only in root node */
	private ICClient _client = null;
	
	/**
	 * Creates root node
	 */
	public FtpFSNode(String pathname, ICClient client) {
		this.pathname = pathname;
		this.name = "/";
		this.directory = true;
		this.parentNode = null;
		this._client = client;
	}
	
	/**
	 * Creates ordinary root
	 * 
	 * @param pathname
	 * @param name
	 * @param directory
	 * @param parent
	 */
	private FtpFSNode(String pathname, String name, boolean directory, FtpFSNode parent) {
		this.pathname = pathname;
		this.name = name;
		this.directory = directory;
		this.parentNode = parent;
	}

	@Override
	public byte[] getContent() {
		if (isDirectory())
			return null;
		
		try {
			return getFileContent();
		} catch (Throwable ex) {
			System.out.println("FSNode: Throwable caught " + ex);
			throw new RuntimeException(ex);
		}
	}
	
	private byte[] getFileContent() throws IOException {
		ICClient client = getLiveClient();

        if (client == null) {
            System.out.println("Was not connected and connection failed.");
            // BUG found by simulation
            return null;
        }

		URL url = new URL(parentNode.pathname);
		client.changeWorkingDirectory(url.getFile());
		// this also works: return client.retrieveFile(name);
		CFileStream stream = client.retrieveFileStream(name);

		if (stream == null) {
			// BUG found in by simulation
			System.out.println("FSNode: retrStream failed");
			return null;
		}

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		int c = stream.read();
		while (c != -1) {
			output.write((byte)c);
			c = stream.read();
		}
		stream.close();
		return output.toByteArray();
	}
	
	private static String SEPARATOR = "/";
	
	private FtpFSNode getRootNode() {
		if (parentNode == null)
			return this;
		
		return parentNode.getRootNode();
	}
	
	private ICClient _getClient() {
		return getRootNode()._client;
	}
	
	private ICClient getLiveClient() throws IOException {
		ICClient client = _getClient();
		// comment out this branch in order to get state exception
		if (!client.isConnected()) {
			URL url = new URL(getRootNode().pathname);
			boolean suc = client.connect(url.getHost(), 21);
			if (!suc) {
				System.out.println("FSNode: connect failed");
				return null;
			}
		}

		// BUG found in by simulation
		if (!client.isLogged()) {
			boolean suc = client.login("anonymous", "osgi.filemanager@kiv.zcu.cz");
			if (!suc) {
				System.out.println("FSNode: login failed");
				return null;
			}
		}
		
		return client;
	}

    @Override
    public IFSNode getSubNode(String filename, boolean fileIsDirectory) {
        FtpFSNode result = new FtpFSNode(joinPath(pathname, filename), filename, fileIsDirectory, this);
        return result;
    }

    private static String joinPath(String pathname, String filename) {
        String newPathName = pathname;
        if (!newPathName.endsWith(SEPARATOR))
            newPathName += SEPARATOR;
        newPathName += filename;
        return newPathName;
    }

    @Override
	public IFSNode[] getDirectoryListing() {
		ArrayList<IFSNode> result = new ArrayList<IFSNode>();
		ICClient client = null;
		try {
			URL url = new URL(pathname);
			client = getLiveClient();
			if (client == null) {
				return null;
			}
			boolean suc = client.changeWorkingDirectory(url.getFile());
			if (!suc) {
				System.out.println("FSNode: cwd failed");
				return null;
			}
			CFtpFile[] files = client.listFiles();
			if (files == null) {
				System.out.println("FSNode: listFiles failed");
				return null;
			}

			for (CFtpFile i : files) {
				String newPathName = joinPath(pathname, i.getName());
				FtpFSNode node = new FtpFSNode(newPathName, i.getName(), i.isDirectory(), this);
				result.add(node);
			}
		} catch (IOException e) {
			e.printStackTrace();
			/*
			if (client != null)
				try {
					//client.disconnect(); // TODO
				} catch (IOException e1) {
					// ignore
				}
			e.printStackTrace();
			*/
		}
		
		return result.toArray(new IFSNode[]{});
	}

	@Override
	public boolean isDirectory() {
		return directory;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getAbsolutePath() {
		return pathname;
	}
	
	public String toString() {
		return String.format("[FtpFile:%s]", getAbsolutePath());
	}

	@Override
	public void _reset() {
		ICClient c = _getClient();
		if (c instanceof IRecreatingCClient) {
			((IRecreatingCClient) c).recreate();
		}
	}
}
