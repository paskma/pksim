package pksim.gui;

import examples.Experiment;
import examples.abreal.Util;
import pksim.core.Event;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame {
	private JButton btnStep;
	private JList<Event> lstEvents;
	private DefaultListModel<Event> modelEvents = new DefaultListModel<Event>();
	private Experiment experiment;
	private WaitingInterceptor interceptor;

	private void setExperiment(Experiment experiment) {
		this.experiment = experiment;
		interceptor = new WaitingInterceptor(experiment.getSimulation());
		experiment.getSimulation().setInterceptor(interceptor);
		experiment.getSimulation().setAllowHistory(true);
		experiment.start();
	}

	private void syncEvents() {
		modelEvents.clear();
		for (Event e : interceptor.getSnapshot().events)
			modelEvents.addElement(e);

		System.out.println("TOTAL " + modelEvents.size());

	}

	private void stepClicked() {
		interceptor.step();
		Util.pause(200);
		syncEvents();
	}

	private Frame(Experiment experiment) {
		setExperiment(experiment);
		setDefaultCloseOperation(Frame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout(10, 10));
		btnStep = new JButton("Step");
		add(btnStep, BorderLayout.NORTH);
		btnStep.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stepClicked();
			}
		});
		lstEvents = new JList<Event>(modelEvents);
		JScrollPane scrollEvents = new JScrollPane(lstEvents);
		scrollEvents.setPreferredSize(new Dimension(400, 300));
		add(scrollEvents, BorderLayout.CENTER);
		syncEvents();
		pack();
	}

	public static void start(final Experiment experiment) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Frame f = new Frame(experiment);

				f.setVisible(true);
			}
		});
	}
}
