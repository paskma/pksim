package pksim.gui;

import pksim.core.Event;
import pksim.core.Interceptor;
import pksim.core.Simulation;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class WaitingInterceptor extends Interceptor {
	Lock lock = new ReentrantLock();
	private Condition c = lock.newCondition();
	private final Simulation simulation;
	private boolean closed = true;

	public static class Snapshot {
		public List<Event> events = new LinkedList<Event>();
	}
	private Snapshot snapshot = new Snapshot();
	public Snapshot getSnapshot() {
		return snapshot;
	}

	private void updateSnapshot() {
		Snapshot result = new Snapshot();
		result.events.addAll(simulation.getCalendar().getPastEvents());
		result.events.addAll(simulation.getCalendar().getFutureEvents());
		snapshot = result;
	}

	public WaitingInterceptor(Simulation simulation) {
		this.simulation = simulation;
	}

	@Override
	public  void atEventStart() {
		synchronized (simulation.getCalendar()) {
			while (closed) {
				try {
					updateSnapshot();
					simulation.getCalendar().wait();
				} catch (InterruptedException e) {
					e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
					break;
				}
			}
			closed = true;
		}
	}

	public  void step() {
		synchronized (simulation.getCalendar()) {
			closed = false;
			simulation.getCalendar().notify();
		}
	}
}
