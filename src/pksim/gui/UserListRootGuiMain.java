package pksim.gui;

import examples.Experiment;
import examples.componentftpsim.UserListRootExperiment;

public class UserListRootGuiMain {
	public static void main(String[] args) {
		Experiment experiment = new UserListRootExperiment();
		Frame.start(experiment);
	}
}
