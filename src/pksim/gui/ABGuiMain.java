package pksim.gui;

import examples.Experiment;
import examples.absim.ABExperiment;

public class ABGuiMain {
	public static void main(String[] args) {
		Experiment experiment = new ABExperiment();
		Frame.start(experiment);
	}
}
