package pksim.gui;

import examples.Experiment;
import examples.componentftpsim.UserRetrXxExperiment;
import pyftpclient_layer.CClient;

public class UserRetrXxCBugGuiMain {
	public static void main(String[] args) {
		Experiment experiment = new UserRetrXxExperiment(true, CClient.NET_TEST_CONFIRMATION_FAIL, false);
		Frame.start(experiment);
	}
}
