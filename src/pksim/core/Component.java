package pksim.core;

public class Component {
	public final int id;
	public final Object object;


	public Component(int id, Object object) {
		this.id = id;
		this.object = object;
	}
}
