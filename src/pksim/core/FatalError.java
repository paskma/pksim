package pksim.core;

public class FatalError extends AssertionError {
	public FatalError() {
	}

	public FatalError(Throwable cause) {
		super(cause);
	}
}
