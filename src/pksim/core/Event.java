package pksim.core;

public class Event implements Comparable {
	public final int timestamp, origin, target;
	public final CrossPoint crossPoint;

	@Override
	public int compareTo(Object o) {
		Event other = (Event) o;
		return Long.compare(timestamp, other.timestamp);
	}

	enum Type {CALL, RETURN}

	public final Type type;

	public Event(Type type, int timestamp, int origin, int target, CrossPoint crossPoint) {
		this.type = type;
		this.timestamp = timestamp;
		this.origin = origin;
		this.target = target;
		this.crossPoint = crossPoint;
	}

	public String toString() {
		String result = "Event ";
		switch (type) {
			case CALL:
				result += String.format("calling %s %d->%d (at %d) [%s]",
						crossPoint.methodName, origin, target, timestamp, crossPoint.threadName);
				break;
			case RETURN:
				result += String.format("returning from %s %d->%d (at %d) [%s]",
						crossPoint.methodName, origin, target, timestamp, crossPoint.threadName);
				break;
			default:
				throw new AssertionError();
		}

		return result;
	}
}
