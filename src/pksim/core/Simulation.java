package pksim.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Every component should have own thread to make asynchronous request-respond via Calendar.
 * Every call is suspended and resumed when the return is scheduled in the calendar.
 */
public class Simulation implements Runnable {
	final Calendar calendar;
	final Map<Integer, Component> components = new HashMap<Integer, Component>();
	final Thread calendarThread = new Thread(this);
	private final static int METHOD_DURATION = 10;
	private boolean allowHistory = false;
	private volatile int loopCounter = 0;
	/**
	 * -1 means infinity
	 */
	public int loopLimit = -1;
	private Interceptor interceptor;

	public Simulation() {
		this.calendar = new Calendar();
		calendarThread.setName("CalendarThread");
	}

	public void setInterceptor(Interceptor interceptor) {
		this.interceptor = interceptor;
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public void addComponent(Component c) {
		components.put(c.id, c);
	}

	@Override
	public void run() {
		try {
			calendarLoop();
		} catch (InterruptedException e) {
		} catch (Error ex) {
			System.out.println("Error in simulation loop #" +loopCounter);
			throw ex;
		}
	}

	private boolean checkLimit(int counter) {
		if (loopLimit < 0)
			return true;

		return counter < loopLimit;
	}

	private void atEventStart() {
		if (interceptor != null)
			interceptor.atEventStart();
	}

	public void calendarLoop() throws InterruptedException {
		while (!calendarThread.isInterrupted() && checkLimit(loopCounter)) {
			synchronized (calendar) {
				while (calendar.isEmpty())
					calendar.wait();

				atEventStart();

				Event event = calendar.takeCurrentEvent();
				System.out.println("#" + loopCounter);
				calendar.currentTime = event.timestamp;
				executeEvent(event);
				if (allowHistory)
					calendar.addPastEvent(event);
				loopCounter++;
			}

		}
	}

	private void executeEvent(Event event) {
		System.out.println("executeEvent: " + event);
		switch (event.type) {
			case CALL:
				executeCallEvent(event);
				break;
			case RETURN:
				executeReturnEvent(event);
				break;
			default:
				throw new FatalError();
		}
	}

	private void executeReturnEvent(Event event) {
		synchronized (event.crossPoint) {
			event.crossPoint.commitResult();
			event.crossPoint.notify();
		}
	}

	private Class[] determineTypes(Object[] args) {
		if (args == null)
			return null;

		Class[] types = new Class[args.length];
		for (int i = 0; i < args.length; i++) {
			types[i] = args[i].getClass();
		}

		return types;
	}

	private void executeCallEvent(Event event) {
		Component targetComponent = components.get(event.target);
		int timestamp = calendar.currentTime + METHOD_DURATION;
		try {
			Method method;
			if (event.crossPoint.arguments == null) {
				method = targetComponent.object.getClass().getMethod(event.crossPoint.methodName);
			} else {
				method = targetComponent.object.getClass().getMethod(
						event.crossPoint.methodName, event.crossPoint.types);
			}
			Object methodResult = method.invoke(targetComponent.object, event.crossPoint.arguments);
			event.crossPoint.setRegularResult(methodResult);
			Event response = new Event(Event.Type.RETURN, timestamp, event.target, event.origin, event.crossPoint);
			calendar.addFutureEvent(response);
		} catch (NoSuchMethodException e) {
			throw new FatalError(e);
		} catch (InvocationTargetException e) {
			event.crossPoint.setExceptionalResult(e.getCause());
			Event response = new Event(Event.Type.RETURN, timestamp, event.target, event.origin, event.crossPoint);
			calendar.addFutureEvent(response);
		} catch (IllegalAccessException e) {
			throw new FatalError(e);
		}
	}


	/**
	 * Used by stubs.
	 */
	public void stubMethodCalled(String methodName, int origin, int target) {
		stubMethodCalled(methodName, null, origin, target);
	}

	public Object stubMethodCalled(String methodName, Object[] args, int origin, int target) {
		return stubMethodCalled(methodName, determineTypes(args), args, origin, target);
	}

	/**
	 * Used by stubs.
	 */
	public Object stubMethodCalled(String methodName, Class[] types, Object[] args, int origin, int target)
	{
		String threadName = Thread.currentThread().getName();
		System.out.println("stubMethodCalled " + methodName + " from comp " + origin + " ["+ threadName + "]");

		CrossPoint crossPoint = new CrossPoint(origin, methodName, types, args, threadName);
		synchronized (calendar) {
			Event event = new Event(Event.Type.CALL, calendar.currentTime, origin, target, crossPoint);
			calendar.addFutureEvent(event);
			calendar.notify();
		}

		Object result;
		Boolean resultIsRegular = null;
		synchronized (crossPoint) {
			while (!crossPoint.isResultCommitted()) {
				System.out.println("no result in comp " + origin + " yet, sleeping [" + threadName + "]");
				try {
					crossPoint.wait();
				} catch (InterruptedException e) {
					throw new FatalError(e);
				}
			}

			resultIsRegular = crossPoint.isRegularResult();
			System.out.println("is taking " + (resultIsRegular ? "regular" : " exceptional ")  + " result [" + threadName + "]");
			if (resultIsRegular)
				result = crossPoint.takeRegularResult();
			else
				result = crossPoint.takeExceptionalResult();
		}

		if (resultIsRegular)
			return result;
		else
			throw new SimulatedRuntimeException((Throwable)result);
	}

	public Thread start() {
		calendarThread.start();
		return calendarThread;
	}

	public void setAllowHistory(boolean allowHistory) {
		this.allowHistory = allowHistory;
	}
}
