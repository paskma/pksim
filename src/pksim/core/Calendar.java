package pksim.core;

import java.util.*;

public class Calendar {
	public volatile int currentTime = 0;
	private LinkedList<Event> futureEvents = new LinkedList<Event>();
	private List<Event> pastEvents = new LinkedList<Event>();

	public void addFutureEvent(Event event) {
		if (event.timestamp < currentTime)
			throw new AssertionError(String.format("Event at %d is from past. Now is %d.", event.timestamp, currentTime));

		int counter = futureEvents.size();
		ListIterator<Event> it = futureEvents.listIterator(futureEvents.size());
		while (it.hasPrevious()) {
			Event i = it.previous();
			if (i.timestamp <= event.timestamp)
				break;
			else {
				System.out.println("skip one");
				counter--;
			}
		}

		futureEvents.add(counter, event);
	}

	public boolean isEmpty() {
		return futureEvents.isEmpty();
	}

	public Event takeCurrentEvent() {
		Event result = futureEvents.remove(0);
		currentTime = result.timestamp;
		return result;
	}

	public void addPastEvent(Event event) {
		pastEvents.add(event);
	}

	public List<Event> getPastEvents() {
		return Collections.unmodifiableList(pastEvents);
	}

	public List<Event> getFutureEvents() {
		return Collections.unmodifiableList(futureEvents);
	}


}
