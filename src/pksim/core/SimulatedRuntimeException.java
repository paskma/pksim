package pksim.core;


public class SimulatedRuntimeException extends RuntimeException {
	public SimulatedRuntimeException(Throwable cause) {
		super(cause);
	}

	public String getMessage() {
		return getCause().getMessage();
	}
}
