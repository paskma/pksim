package pksim.core;

public class CrossPoint {
	public final String methodName;
	public final Class[] types;
	public final Object[] arguments;
	private Object regularResult = null;
	private Throwable exceptionalResult = null;
	public final String threadName;

	private boolean committed = false;
	private final int componentId;

	public CrossPoint(int componentId, String methodName, Class[] types, Object[] arguments, String threadName) {
		this.componentId = componentId;
		this.methodName = methodName;
		this.types = types;
		this.arguments = arguments;
		this.threadName = threadName;
	}

	public void setRegularResult(Object regularResult) {
		if (this.regularResult != null || this.exceptionalResult != null)
			throw new AssertionError("Result is not null, comp " + componentId + " , isset " + committed);

		this.regularResult = regularResult;
	}

	public void setExceptionalResult(Throwable exceptionalResult) {
		if (exceptionalResult == null)
			throw new AssertionError("Null exception not allowed.");
		if (this.regularResult != null || this.exceptionalResult != null)
			throw new AssertionError("Result is not null, comp " + componentId + " , isset " + committed);

		this.exceptionalResult = exceptionalResult;
	}

	public void commitResult() {
		committed = true;
	}

	public boolean isResultCommitted() {
		return committed;
	}

	public boolean isRegularResult() {
		if (!committed)
			throw new AssertionError("Not committed.");

		boolean isRegular = (exceptionalResult == null); // valid regular result migth be null

		return isRegular;
	}

	public Object takeRegularResult() {
		if (!committed)
			throw new AssertionError("Result is not committed, comp id " + componentId);

		System.out.println("taken from comp id " + componentId);
		Object result = this.regularResult;
		this.regularResult = null;
		committed = false;
		return result;
	}

	public Throwable takeExceptionalResult() {
		if (!committed)
			throw new AssertionError("Result is not committed, comp id " + componentId);

		System.out.println("ex taken from comp id " + componentId);
		Throwable result = this.exceptionalResult;
		this.exceptionalResult = null;
		committed = false;
		return result;
	}
}
