package examples.absim;

import examples.abreal.B;
import pksim.core.Simulation;

public class BStub implements B {
	private final Simulation simulation;
	private final int originComponent, targetComponent;

	public BStub(Simulation simulation, int originComponent, int targetComponent) {
		this.simulation = simulation;
		this.originComponent = originComponent;
		this.targetComponent = targetComponent;
	}

	@Override
	public void doSomething() {
		simulation.stubMethodCalled("doSomething", originComponent, targetComponent);
	}

	@Override
	public int plus(Integer a, Integer b) {
		Object[] args = {a, b};
		return (Integer)simulation.stubMethodCalled("plus", args, originComponent, targetComponent);
	}
}
