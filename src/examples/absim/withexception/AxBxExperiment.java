package examples.absim.withexception;

import examples.Experiment;
import examples.abreal.AImpl;
import examples.abreal.BImpl;
import examples.abreal.withexception.AxImpl;
import examples.abreal.withexception.BxImpl;
import examples.absim.BStub;
import pksim.core.Component;
import pksim.core.Simulation;

public class AxBxExperiment extends Experiment {
	private static final int A_ID = 1, B_ID = 2;
	private AxImpl aImpl;

	public AxBxExperiment() {
		simulation = new Simulation();

		BStub bStub = new BStub(simulation, A_ID, B_ID);
		aImpl = new AxImpl(bStub);
		Component cA = new Component(A_ID, aImpl);
		simulation.addComponent(cA);

		BxImpl bImpl = new BxImpl();
		Component cB = new Component(B_ID, bImpl);
		simulation.addComponent(cB);
	}

	public void start() {
		simulation.start();
		aImpl.start(A_ID);
	}
}
