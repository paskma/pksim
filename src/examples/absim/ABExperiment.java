package examples.absim;

import examples.Experiment;
import examples.abreal.AImpl;
import examples.abreal.BImpl;
import pksim.core.Component;
import pksim.core.Simulation;

public class ABExperiment extends Experiment {
	private static final int A_ID = 1, B_ID = 2;
	private AImpl aImpl;

	public ABExperiment() {
		simulation = new Simulation();

		BStub bStub = new BStub(simulation, A_ID, B_ID);
		aImpl = new AImpl(bStub);
		Component cA = new Component(A_ID, aImpl);
		simulation.addComponent(cA);

		BImpl bImpl = new BImpl();
		Component cB = new Component(B_ID, bImpl);
		simulation.addComponent(cB);
	}

	public void start() {
		simulation.start();
		aImpl.start(A_ID);
	}
}
