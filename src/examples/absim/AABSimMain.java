package examples.absim;

import examples.abreal.AImpl;
import examples.abreal.BImpl;
import pksim.core.Component;
import pksim.core.Simulation;

public class AABSimMain {
	private static final int A1_ID = 1, A2_ID = 2, B_ID = 3;

	public static void main(String[] args) {
		Simulation simulation = new Simulation();

		BStub bStub1 = new BStub(simulation, A1_ID, B_ID);
		BStub bStub2 = new BStub(simulation, A2_ID, B_ID);

		AImpl aImpl1 = new AImpl(bStub1);
		Component cA1 = new Component(A1_ID, aImpl1);
		simulation.addComponent(cA1);

		AImpl aImpl2 = new AImpl(bStub2);
		Component cA2 = new Component(A2_ID, aImpl2);
		simulation.addComponent(cA2);

		BImpl bImpl = new BImpl();
		Component cB = new Component(B_ID, bImpl);
		simulation.addComponent(cB);

		simulation.start();
		aImpl1.start(A1_ID);
		aImpl2.start(A2_ID);

	}
}
