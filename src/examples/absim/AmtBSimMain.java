package examples.absim;

import examples.abreal.AmtImpl;
import examples.abreal.BImpl;
import pksim.core.Component;
import pksim.core.Simulation;

public class AmtBSimMain {
	private static final int A_ID = 1, B_ID = 3;

	public static void main(String[] args) {
		Simulation simulation = new Simulation();

		BStub bStub = new BStub(simulation, A_ID, B_ID);
		AmtImpl aImpl = new AmtImpl(bStub);
		Component cA = new Component(A_ID, aImpl);
		simulation.addComponent(cA);

		BImpl bImpl = new BImpl();
		Component cB = new Component(B_ID, bImpl);
		simulation.addComponent(cB);

		simulation.start();
		aImpl.start(10, 20);
	}
}
