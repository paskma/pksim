package examples.abreal;

public class AmtImpl {
	private final B b;

	public AmtImpl(B b) {
		this.b = b;
	}

	private void loop(int i1, int i2) {
		for(;;) {
			b.doSomething();
			int result = b.plus(i1, i2); if (result != i1+i2) throw new AssertionError("Wrong result " + result);
			System.out.println("..result is " + result + " [" + Thread.currentThread().getName() + "]");
			//Util.pause(500);
		}
	}

	public void start(int threadId1, int threadId2) {
		Thread t1 = new Thread() {
			@Override
			public void run() {
				loop(1,2);
			}
		};
		t1.setName("AMtImplThread-" + threadId1);
		t1.start();

		Thread t2 = new Thread() {
			@Override
			public void run() {
				loop(4,5);
			}
		};
		t2.setName("AMtImplThread-" + threadId2);
		t2.start();
	}
}
