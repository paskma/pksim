package examples.abreal;

public class ABRealMain {
	public static void main(String[] args) {
		B b = new BImpl();
		AImpl a = new AImpl(b);
		a.start(1);
	}
}
