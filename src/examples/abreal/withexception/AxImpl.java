package examples.abreal.withexception;

import examples.abreal.B;

public class AxImpl implements Runnable {
	private final B b;
	private Thread t;
	public int loopLimit = -1;

	public AxImpl(B b) {
		this.b = b;
	}

	private boolean checkLimit(int counter) {
		if (loopLimit < 0)
			return true;

		return counter < loopLimit;
	}

	@Override
	public void run() {
		int counter = 0;
		while (!t.isInterrupted() && checkLimit(counter)) {
			System.out.println("@" + counter + "[" + Thread.currentThread().getName() + "]");
			RuntimeException r = null;
			try {
				b.doSomething();
			} catch (RuntimeException ex) {
				r = ex;
			}
			if (r == null) {
				throw new AssertionError("No exception in B.doSomething.");
			}
			System.out.println("Message is '" + r.getMessage() + "' [" + Thread.currentThread().getName() + "]");
			//Util.pause(500);
			counter++;
		}
	}

	public Thread start(int threadId) {
		t = new Thread(this);
		t.setName("AImplThread-" + threadId);
		t.start();
		return t;
	}
}
