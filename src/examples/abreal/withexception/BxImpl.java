package examples.abreal.withexception;

import examples.abreal.B;

public class BxImpl implements B {
	@Override
	public void doSomething() {
		System.out.println("B.doSomething invoked");
		throw new RuntimeException("Testing interface for exceptions.");
	}

	@Override
	public int plus(Integer a, Integer b) {
		System.out.println("B.plus invoked");
		return a + b;
	}
}
