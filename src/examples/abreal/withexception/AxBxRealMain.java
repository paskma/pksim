package examples.abreal.withexception;

import examples.abreal.B;

public class AxBxRealMain {
	public static void main(String[] args) {
		B b = new BxImpl();
		AxImpl a = new AxImpl(b);
		a.start(1);
	}
}
