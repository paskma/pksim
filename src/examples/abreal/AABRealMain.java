package examples.abreal;

public class AABRealMain {
	public static void main(String[] args) {
		B b = new BImpl();
		AImpl a1 = new AImpl(b);
		AImpl a2 = new AImpl(b);
		a1.start(1);
		a2.start(2);
		System.out.println("Main done.");
	}
}
