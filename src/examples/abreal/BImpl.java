package examples.abreal;

public class BImpl implements B {
	@Override
	public void doSomething() {
		System.out.println("B.doSomething invoked");
	}

	@Override
	public int plus(Integer a, Integer b) {
		System.out.println("B.plus invoked");
		return a + b;
	}
}
