package examples.abreal;

public class AImpl implements Runnable {
	private final B b;
	private Thread t;
	public int loopLimit = -1;

	public AImpl(B b) {
		this.b = b;
	}

	private boolean checkLimit(int counter) {
		if (loopLimit < 0)
			return true;

		return counter < loopLimit;
	}

	@Override
	public void run() {
		int counter = 0;
		while (!t.isInterrupted() && checkLimit(counter)) {
			System.out.println("@" + counter + "[" + Thread.currentThread().getName() + "]");
			b.doSomething();
			int result = b.plus(1,2); if (result != 3) throw new AssertionError("Wrong result " + result);
			System.out.println("..result is " + result + " [" + Thread.currentThread().getName() + "]");
			//Util.pause(500);
			counter++;
		}
	}

	public Thread start(int threadId) {
		t = new Thread(this);
		t.setName("AImplThread-" + threadId);
		t.start();
		return t;
	}
}
