package examples.componentftpreal;

import filemanager.ftpfsaccess.FtpFSAccess;

public class UserListRootMain {
	public static void main(String [] args) {
		FtpFSAccess ftpFs = new FtpFSAccess();
		UserListRoot user = new UserListRoot(ftpFs);
		user.start(1);
	}
}
