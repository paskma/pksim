package examples.componentftpreal;

import examples.componentftpsim.RecreatingCClient;
import pyftpclient_layer.CClientFactoryImpl;
import pyftpclient_layer.ICClient;

/**
 *
 */
public class RecreatingCClientFactoryImpl extends CClientFactoryImpl {
	public RecreatingCClientFactoryImpl(int networkType) {
		super(networkType);
	}

	@Override
	public ICClient createCClient() {
		RecreatingCClient result = new RecreatingCClient(networkType);
		result.setDataTransferConfirmationBug(dataTransferConfirmationBug);
		result.setPasvResponseReadingBug(pasvResponseReadingBug);
		return result;
	}
}
