package examples.componentftpreal;

import examples.abreal.Util;
import filemanager.utils.IFSNode;
import filemanager.utils.IFileAccess;

import java.nio.charset.Charset;

public class UserRetrXx implements Runnable {
	private int loopLimit = -1;
	private Thread t;
	private IFileAccess fileAccess;
	private boolean haltOnCompleteScenario = false;

	public void setHaltOnCompleteScenario(boolean haltOnCompleteScenario) {
		this.haltOnCompleteScenario = haltOnCompleteScenario;
	}

	public UserRetrXx(IFileAccess fileAccess) {
		this.fileAccess = fileAccess;
	}

	private boolean checkLimit(int counter) {
		if (loopLimit < 0)
			return true;

		return counter < loopLimit;
	}

	/**
	 * Returns true if scenario is executed successfully.
	 */
	private boolean iteration(IFSNode node) {
		System.out.println(node.getAbsolutePath());
		IFSNode[] rootDir = node.getDirectoryListing();

		if (rootDir == null) {
			System.out.println("Listing returned null."); // something happened lower
			return false;
		}

		IFSNode xxNode = null;
		for (IFSNode i : rootDir) {
			if (i.getName().equals("xx")) {
				xxNode = i;
				break;
			}
		}

		for (int i = 0; i < 2; i++) {
			byte[] content = xxNode.getContent();
			if (content == null) {
				System.out.println("Content retr fail.");
				continue;
			}
			String strContent = new String(content, Charset.defaultCharset());
			System.out.println(String.format("Content is #%d:", i));
			System.out.println(strContent);
			System.err.println("OK");
			return true;
		}

		return false;
	}

	@Override
	public void run() {
		IFSNode node = fileAccess.getFSNode("ftp://ftp.zcu.cz");
		int counter = 0;
		while (!t.isInterrupted() && checkLimit(counter)) {
			boolean suc = iteration(node);
			//Util.pause(500);
			counter++;
			System.out.println("@" + counter);
			if (suc && haltOnCompleteScenario) {
				System.out.println("haltOnCompleteScenario");
				break;
			}
		}
	}

    /**
     * For the purpose of the paper.
     * @param ftpService
     */
    private void articleRun (IFileAccess ftpService) {
        IFSNode node = ftpService.getFSNode("ftp://example.com");
        IFSNode[] rootDir = node.getDirectoryListing();
        assert rootDir != null;

        IFSNode exampleFileNode = null;
        for (IFSNode i : rootDir) {
            if (i.getName().equals("example.txt")) {
                exampleFileNode = i;
                break;
            }
        }

        byte[] content = exampleFileNode.getContent();
        assert content != null;
    }

	public Thread start(int threadId) {
		t = new Thread(this);
		t.setName("UserRetrXxThread-" + threadId);
		t.start();
		return t;
	}
}
