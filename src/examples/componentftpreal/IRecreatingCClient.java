package examples.componentftpreal;

import pyftpclient_layer.ICClient;

/**
 *
 */
public interface IRecreatingCClient extends ICClient {
	void recreate();
}
