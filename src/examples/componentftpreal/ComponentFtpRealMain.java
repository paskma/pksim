package examples.componentftpreal;

import filemanager.ftpfsaccess.FtpFSAccess;
import filemanager.utils.IFSNode;
import pyftpclient_layer.CClient;
import pyftpclient_layer.CClientFactoryImpl;

/**
 * Use FtpFileAccess to get content from real server.
 */
public class ComponentFtpRealMain {
	public static void main(String[] args) {
		FtpFSAccess ftpFs = new FtpFSAccess();
        ftpFs.setCClientFactory(new CClientFactoryImpl(CClient.NET_WILD));
		IFSNode node = ftpFs.getFSNode("ftp://ftp.zcu.cz/");
		System.out.println(node.getAbsolutePath());
		for (IFSNode i : node.getDirectoryListing()) {
			System.out.println(i.getName());
		}

	}
}
