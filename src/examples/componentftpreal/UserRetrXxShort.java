package examples.componentftpreal;

import filemanager.utils.IFSNode;
import filemanager.utils.IFileAccess;

import java.nio.charset.Charset;

public class UserRetrXxShort implements Runnable {
	private int loopLimit = -1;
	private Thread t;
	private IFileAccess fileAccess;
	private boolean haltOnCompleteScenario = false;

	public void setHaltOnCompleteScenario(boolean haltOnCompleteScenario) {
		this.haltOnCompleteScenario = haltOnCompleteScenario;
	}

	public UserRetrXxShort(IFileAccess fileAccess) {
		this.fileAccess = fileAccess;
	}

	private boolean checkLimit(int counter) {
		if (loopLimit < 0)
			return true;

		return counter < loopLimit;
	}

	/**
	 * Returns true if scenario is executed successfully.
	 */
	private boolean iteration(IFSNode node) {

		IFSNode xxNode = node.getSubNode("xx", false);
		byte[] content = xxNode.getContent();
		if (content == null) {
			System.out.println("Content retr fail.");
		}

		content = xxNode.getContent();
		if (content == null) {
			System.out.println("Content retr fail, 2.");
		}

		return false;
	}

	@Override
	public void run() {
		int counter = 0;
		IFSNode node = fileAccess.getFSNode("ftp://ftp.zcu.cz/");
		if (node == null)
			return;
		while (!t.isInterrupted() && checkLimit(counter)) {
			boolean suc = iteration(node);
			node._reset();
			//Util.pause(500);
			counter++;
			System.out.println("@@" + counter);
			if (suc && haltOnCompleteScenario) {
				System.out.println("haltOnCompleteScenario");
				break;
			}
		}
	}

	public Thread start(int threadId) {
		t = new Thread(this);
		t.setDaemon(true);
		t.setName("UserRetrXxThread-" + threadId);
		t.start();
		return t;
	}

	public void joinThread() {
		try {
			t.join();
		} catch (InterruptedException e) {
			System.out.println("Unexpected interrupted exception.");
		}
	}
}
