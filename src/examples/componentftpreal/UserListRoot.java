package examples.componentftpreal;

import examples.abreal.Util;
import filemanager.utils.IFSNode;
import filemanager.utils.IFileAccess;

public class UserListRoot implements Runnable {
	private int loopLimit = -1;
	private Thread t;
	private IFileAccess fileAccess;

	public UserListRoot(IFileAccess fileAccess) {
		this.fileAccess = fileAccess;
	}

	private boolean checkLimit(int counter) {
		if (loopLimit < 0)
			return true;

		return counter < loopLimit;
	}

	private void iteration(IFSNode node) {
		System.out.println(node.getAbsolutePath());
		for (IFSNode i : node.getDirectoryListing()) {
			System.out.println(i.getName());
		}
	}

	@Override
	public void run() {
		IFSNode node = fileAccess.getFSNode("ftp://ftp.zcu.cz/");
		int counter = 0;
		while (!t.isInterrupted() && checkLimit(counter)) {
			iteration(node);
			Util.pause(500);
			counter++;
		}
	}

	public Thread start(int threadId) {
		t = new Thread(this);
		t.setName("AImplThread-" + threadId);
		t.start();
		return t;
	}
}
