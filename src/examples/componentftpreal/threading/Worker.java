package examples.componentftpreal.threading;

import filemanager.utils.IFSNode;

/**
 */
public class Worker extends Thread {
	private final IFSNode node;

	public Worker(IFSNode node) {
		super("WorkerTh");
		this.node = node;
	}

	public void run() {
		try {
			node.getContent();
		} catch (RuntimeException ex) {
			System.out.println("Expected:" + ex.getCause() + " " + ex.getCause().getClass().getCanonicalName());
		}
	}
}
