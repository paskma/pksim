package examples.componentftpreal.threading;

import examples.componentftpreal.RecreatingCClientFactoryImpl;
import examples.componentftpreal.UserRetrXxShort;
import filemanager.ftpfsaccess.FtpFSAccess;
import pyftpclient_layer.CClient;

public class UserMtMain {
	public static void main(String [] args) {
		FtpFSAccess ftpFs = new FtpFSAccess();
		RecreatingCClientFactoryImpl clientFactory = new RecreatingCClientFactoryImpl(CClient.NET_TEST);
		//not setting clientFactory.setDataTransferConfirmationBug(true);
		ftpFs.setCClientFactory(clientFactory);
		UserMt user = new UserMt(ftpFs);
		user.start(1);
		user.joinThread();
	}
}
