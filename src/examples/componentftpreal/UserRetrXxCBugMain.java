package examples.componentftpreal;

import filemanager.ftpfsaccess.FtpFSAccess;
import pyftpclient_layer.CClient;
import pyftpclient_layer.CClientFactoryImpl;

public class UserRetrXxCBugMain {
	public static void main(String [] args) {
		FtpFSAccess ftpFs = new FtpFSAccess();
		CClientFactoryImpl clientFactory = new CClientFactoryImpl(CClient.NET_TEST_CONFIRMATION_FAIL);
		clientFactory.setDataTransferConfirmationBug(true);
		ftpFs.setCClientFactory(clientFactory);
		UserRetrXx user = new UserRetrXx(ftpFs);
		user.start(1);
	}
}
