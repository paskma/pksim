package examples.componentftpsim;

import pyftpclient_layer.CClient;

/**
 * This is expected to fail with exception.
 *
 * Caused by: java.lang.NullPointerException
 *  at pypy.ftpclient.client.Client_81.oretrieveFileStream(Client_81.j:1897)
 *  at pyftpclient_layer.CClient.retrieveFileStream(CClient.java:172)
 */
public class UserRetrXxExperimentCutShortCBugMain {
	public static void main(String[] args) {
		boolean dataTransferConfirmationBug = false;
		boolean pasvResponseReadingBug = true;
		UserRetrXxShortExperiment userExperiment = new UserRetrXxShortExperiment(
				dataTransferConfirmationBug, pasvResponseReadingBug, CClient.NET_LINE_CUT_RAND, false);
		userExperiment.start();
	}
}
