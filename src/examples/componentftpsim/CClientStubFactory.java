package examples.componentftpsim;

import pksim.core.Simulation;
import pyftpclient_layer.CClientFactory;
import pyftpclient_layer.ICClient;

public class CClientStubFactory implements CClientFactory {
	private final int origin, target;
	private final Simulation simulation;
	private int counter = 0;

	public CClientStubFactory(int origin, int target, Simulation simulation) {
		this.origin = origin;
		this.target = target;
		this.simulation = simulation;
	}

	@Override
	public ICClient createCClient() {
		if (counter > 0)
			throw new AssertionError("In simulation this should be called only once.");

		++counter;

		return new CClientStub(simulation, origin, target);
	}
}
