package examples.componentftpsim;

import pyftpclient_layer.CClient;

/**
 * This is expected to run without an exception.
 */
public class UserRetrXxExperimentCutShortMain {
	public static void main(String[] args) {
		boolean dataTransferConfirmationBug = false;
		boolean pasvResponseReadingBug = false;
		UserRetrXxShortExperiment userExperiment = new UserRetrXxShortExperiment(
				dataTransferConfirmationBug, pasvResponseReadingBug, CClient.NET_LINE_CUT_RAND, false);
		userExperiment.start();
	}
}
