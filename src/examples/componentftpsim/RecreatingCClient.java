package examples.componentftpsim;

import examples.componentftpreal.IRecreatingCClient;
import pyftpclient_layer.CClient;
import pyftpclient_layer.CFileStream;
import pyftpclient_layer.CFtpFile;
import pyftpclient_layer.ICClient;

/**
 * Inner cclient is re-created with every login.
 */
public class RecreatingCClient implements IRecreatingCClient {
	private CClient client;
	private final int network;
	private boolean dataTransferConfirmationBug, pasvResponseReadingBug;


	public RecreatingCClient(int network) {
		this.network = network;
		recreate();
	}

	@Override
	public void setDataTransferConfirmationBug(boolean b) {
		this.dataTransferConfirmationBug = b;
		client.setDataTransferConfirmationBug(b);
	}

	@Override
	public void setPasvResponseReadingBug(boolean b) {
		this.pasvResponseReadingBug = b;
		client.setPasvResponseReadingBug(b);
	}

	@Override
	public boolean connect(String host, int port) {
		return client.connect(host, port);
	}

	@Override
	public boolean isConnected() {
		return client.isConnected();
	}

	@Override
	public boolean isLogged() {
		return client.isLogged();
	}

	@Override
	public boolean login(String s, String s2) {
		return client.login(s, s2);
	}

	@Override
	public CFtpFile[] listFiles() {
		return client.listFiles();
	}

	@Override
	public boolean changeWorkingDirectory(String s) {
		return client.changeWorkingDirectory(s);
	}

	@Override
	public boolean logout() {
		return client.logout();
	}

	@Override
	public byte[] retrieveFile(String s) {
		return client.retrieveFile(s);
	}

	@Override
	public CFileStream retrieveFileStream(String s) {
		return client.retrieveFileStream(s);
	}

	@Override
	public boolean deleteFile(String s) {
		return client.deleteFile(s);
	}

	public void recreate() {
		System.out.println("ZZZ RECREATION");
		client = new CClient(network);
		client.setDataTransferConfirmationBug(dataTransferConfirmationBug);
		client.setPasvResponseReadingBug(pasvResponseReadingBug);
	}
}
