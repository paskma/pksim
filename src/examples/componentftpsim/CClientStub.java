package examples.componentftpsim;

import pksim.core.Simulation;
import pyftpclient_layer.CFileStream;
import pyftpclient_layer.CFtpFile;
import pyftpclient_layer.ICClient;

public class CClientStub implements ICClient {
	private final Simulation simulation;
	private final int originComponent, targetComponent;

	public CClientStub(Simulation simulation, int originComponent, int targetComponent) {
		this.simulation = simulation;
		this.originComponent = originComponent;
		this.targetComponent = targetComponent;
	}

	protected Object call(String name, Object[] args) {
		return simulation.stubMethodCalled(name, args, originComponent, targetComponent);
	}

	protected Object call(String name, Class[] types, Object[] args) {
		return simulation.stubMethodCalled(name, types, args, originComponent, targetComponent);
	}

	@Override
	public void setDataTransferConfirmationBug(boolean b) {
		call("setDataTransferConfirmationBug", new Object[] {b});
	}

	@Override
	public void setPasvResponseReadingBug(boolean b) {
		call("setPasvResponseReadingBug", new Object[] {b});
	}

	@Override
	public boolean connect(String s, int i) {
		return (Boolean)call("connect", new Class[] {String.class, int.class},  new Object[] {s, i});
	}

	@Override
	public boolean isConnected() {
		return (Boolean)call("isConnected", new Object[] {});
	}

	@Override
	public boolean isLogged() {
		return (Boolean)call("isLogged", new Object[] {});
	}

	@Override
	public boolean login(String s, String s1) {
		return (Boolean)call("login", new Object[] {s, s1});
	}

	@Override
	public CFtpFile[] listFiles() {
		return (CFtpFile[])call("listFiles", new Object[] {});
	}

	@Override
	public boolean changeWorkingDirectory(String s) {
		return (Boolean)call("changeWorkingDirectory", new Object[] {s});
	}

	@Override
	public boolean logout() {
		return (Boolean)call("logout", new Object[] {});
	}

	@Override
	public byte[] retrieveFile(String s) {
		return (byte[])call("retrieveFile", new Object[] {s});
	}

	@Override
	public CFileStream retrieveFileStream(String s) {
		return (CFileStream)call("retrieveFileStream", new Object[] {s});
	}

	@Override
	public boolean deleteFile(String s) {
		return (Boolean)call("deleteFile", new Object[] {s});
	}
}
