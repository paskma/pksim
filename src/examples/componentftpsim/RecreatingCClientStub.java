package examples.componentftpsim;

import examples.componentftpreal.IRecreatingCClient;
import pksim.core.Simulation;
import pyftpclient_layer.CFileStream;
import pyftpclient_layer.CFtpFile;
import pyftpclient_layer.ICClient;

public class RecreatingCClientStub extends CClientStub implements IRecreatingCClient {

	public RecreatingCClientStub(Simulation simulation, int originComponent, int targetComponent) {
		super(simulation, originComponent, targetComponent);

	}

	@Override
	public void recreate() {
		call("recreate", new Object[] {});
	}
}
