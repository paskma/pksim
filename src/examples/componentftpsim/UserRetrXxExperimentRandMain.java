package examples.componentftpsim;

import pyftpclient_layer.CClient;

/**
 * This is expected not to fail with exception.
 */
public class UserRetrXxExperimentRandMain {
	public static void main(String[] args) {
		UserRetrXxExperiment userExperiment = new UserRetrXxExperiment(false, CClient.NET_CODE_RAND, true);
		userExperiment.start();
	}
}
