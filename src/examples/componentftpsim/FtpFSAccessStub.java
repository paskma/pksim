package examples.componentftpsim;

import filemanager.utils.IFSNode;
import filemanager.utils.IFileAccess;
import pksim.core.Simulation;

public class FtpFSAccessStub implements IFileAccess {
	private final int origin, target;
	private final Simulation simulation;

	public FtpFSAccessStub(int origin, int target, Simulation simulation) {
		this.origin = origin;
		this.target = target;
		this.simulation = simulation;
	}

	@Override
	public IFSNode getFSNode(String pathname) {
		return (IFSNode)simulation.stubMethodCalled("getFSNode", new Object[] {pathname}, origin, target);
	}

	@Override
	public boolean deleteFSNode(String pathname, boolean deleteSubdir) {
		throw new AssertionError("Unexpected call.");
	}

	@Override
	public boolean copyFSNode(String sourceName, String destName) {
		throw new AssertionError("Unexpected call.");
	}

	@Override
	public boolean renameFSNode(String oldName, String newName) {
		throw new AssertionError("Unexpected call.");
	}
}
