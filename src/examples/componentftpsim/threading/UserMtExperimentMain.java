package examples.componentftpsim.threading;

import examples.componentftpsim.UserRetrXxShortExperiment;
import pyftpclient_layer.CClient;

/**
 * This is expected to run without exceptions.
 */
public class UserMtExperimentMain {
	public static void main(String[] args) {
		boolean dataTransferConfirmationBug = false;
		boolean pasvResponseReadingBug = false;
		UserMtExperiment userExperiment = new UserMtExperiment(
				dataTransferConfirmationBug, pasvResponseReadingBug, CClient.NET_TEST, false);
		userExperiment.start();
	}
}
