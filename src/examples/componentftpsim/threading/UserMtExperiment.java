package examples.componentftpsim.threading;

import examples.Experiment;
import examples.componentftpreal.UserRetrXxShort;
import examples.componentftpreal.threading.UserMt;
import examples.componentftpsim.FtpFSAccessStub;
import examples.componentftpsim.RecreatingCClient;
import examples.componentftpsim.RecreatingCClientStubFactory;
import filemanager.ftpfsaccess.FtpFSAccess;
import pksim.core.Component;
import pksim.core.Simulation;

/**
 * Components registered in simulation: User, FtpFSAccess, CClient
 * <p/>
 * User has reference to FtpFSAccessStub that accesses FtpFSAccess via simulation events.
 * FtpFSAccess has reference to CClientStubFactory that returns CClientStub that accesses CClient via simulation events.
 */
public class UserMtExperiment extends Experiment {
	private static final int User_ID = 1, FtpFSAccess_ID = 2, CClient_ID = 3;
	private UserMt user;

	public UserMtExperiment(boolean dataTransferConfirmationBug, boolean pasvResponseReadingBug, int networkType, boolean haltOnCompleteScenario) {
		simulation = new Simulation();

		FtpFSAccessStub ftpFSAccessStub = new FtpFSAccessStub(User_ID, FtpFSAccess_ID, simulation);

		user = new UserMt(ftpFSAccessStub);
		user.setHaltOnCompleteScenario(haltOnCompleteScenario);
		Component userComponent = new Component(User_ID, user);
		simulation.addComponent(userComponent);

		RecreatingCClient recreatingCClient = new RecreatingCClient(networkType);
		recreatingCClient.setDataTransferConfirmationBug(dataTransferConfirmationBug);
		recreatingCClient.setPasvResponseReadingBug(pasvResponseReadingBug);
		Component cClientComponent = new Component(CClient_ID, recreatingCClient);
		simulation.addComponent(cClientComponent);

		FtpFSAccess ftpFSAccess = new FtpFSAccess();
		ftpFSAccess.setCClientFactory(new RecreatingCClientStubFactory(FtpFSAccess_ID, CClient_ID, simulation));
		Component ftpFSAccessComponent = new Component(FtpFSAccess_ID, ftpFSAccess);
		simulation.addComponent(ftpFSAccessComponent);
	}

	@Override
	public void start() {
		simulation.start();
		user.start(User_ID);
	}
}
