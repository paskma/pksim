package examples.componentftpsim;

import examples.Experiment;
import examples.componentftpreal.UserListRoot;
import filemanager.ftpfsaccess.FtpFSAccess;
import pksim.core.Component;
import pksim.core.Simulation;
import pyftpclient_layer.CClient;

public class UserListRootExperiment extends Experiment {
	private static final int User_ID = 1, FtpFSAccess_ID = 2, CClient_ID = 3;
	private UserListRoot user;

	public UserListRootExperiment() {
		simulation = new Simulation();

		FtpFSAccessStub ftpFSAccessStub = new FtpFSAccessStub(User_ID, FtpFSAccess_ID, simulation);

		user = new UserListRoot(ftpFSAccessStub);
		Component userComponent = new Component(User_ID, user);
		simulation.addComponent(userComponent);

		CClient cClient = new CClient(CClient.NET_TEST);
		Component cClientComponent = new Component(CClient_ID, cClient);
		simulation.addComponent(cClientComponent);

		FtpFSAccess ftpFSAccess = new FtpFSAccess();
		ftpFSAccess.setCClientFactory(new CClientStubFactory(FtpFSAccess_ID, CClient_ID, simulation));
		Component ftpFSAccessComponent = new Component(FtpFSAccess_ID, ftpFSAccess);
		simulation.addComponent(ftpFSAccessComponent);
	}

	@Override
	public void start() {
		simulation.start();
		user.start(User_ID);
	}

	public static void main(String[] args) {
		UserListRootExperiment userExperiment = new UserListRootExperiment();
		userExperiment.start();
	}
}
