package examples.componentftpsim;

import examples.Experiment;
import examples.componentftpreal.UserListRoot;
import examples.componentftpreal.UserRetrXx;
import filemanager.ftpfsaccess.FtpFSAccess;
import pksim.core.Component;
import pksim.core.Simulation;
import pyftpclient_layer.CClient;

/**
 * Components registered in simulation: User, FtpFSAccess, CClient
 *
 * User has reference to FtpFSAccessStub that accesses FtpFSAccess via simulation events.
 * FtpFSAccess has reference to CClientStubFactory that returns CClientStub that accesses CClient via simulation events.
 *
 */
public class UserRetrXxExperiment extends Experiment {
	private static final int User_ID = 1, FtpFSAccess_ID = 2, CClient_ID = 3;
	private UserRetrXx user;

	public UserRetrXxExperiment(boolean dataTransferConfirmationBug, int networkType, boolean haltOnCompleteScenario) {
		simulation = new Simulation();

		FtpFSAccessStub ftpFSAccessStub = new FtpFSAccessStub(User_ID, FtpFSAccess_ID, simulation);

		user = new UserRetrXx(ftpFSAccessStub);
		user.setHaltOnCompleteScenario(haltOnCompleteScenario);
		Component userComponent = new Component(User_ID, user);
		simulation.addComponent(userComponent);

		CClient cClient = new CClient(networkType);
		cClient.setDataTransferConfirmationBug(dataTransferConfirmationBug);
		Component cClientComponent = new Component(CClient_ID, cClient);
		simulation.addComponent(cClientComponent);

		FtpFSAccess ftpFSAccess = new FtpFSAccess();
		ftpFSAccess.setCClientFactory(new CClientStubFactory(FtpFSAccess_ID, CClient_ID, simulation));
		Component ftpFSAccessComponent = new Component(FtpFSAccess_ID, ftpFSAccess);
		simulation.addComponent(ftpFSAccessComponent);
	}

	@Override
	public void start() {
		simulation.start();
		user.start(User_ID);
	}
}
