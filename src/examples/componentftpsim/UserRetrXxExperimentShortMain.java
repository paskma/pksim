package examples.componentftpsim;

import pyftpclient_layer.CClient;

/**
 * This is expected to run without exceptions.
 */
public class UserRetrXxExperimentShortMain {
	public static void main(String[] args) {
		boolean dataTransferConfirmationBug = false;
		boolean pasvResponseReadingBug = false;
		UserRetrXxShortExperiment userExperiment = new UserRetrXxShortExperiment(
				dataTransferConfirmationBug, pasvResponseReadingBug, CClient.NET_TEST, false);
		userExperiment.start();
	}
}
