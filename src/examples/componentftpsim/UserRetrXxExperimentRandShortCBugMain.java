package examples.componentftpsim;

import pyftpclient_layer.CClient;

/**
 * This is expected to fail with exception.
 * Caused by: InstanceWrapper('ftpclient.statemachine.StateException',
 * {"omessage":"\x53\x74\x61\x74\x65\x20\x69\x73\x20\x35\x2c\x20\x73\x68\x6f\x75\x6c\x64\x20\x62\x65\x20\x33\x2e"})
 * at pypy.ftpclient.statemachine.StateMachine_82.o_assertState(StateMachine_82.j:854)
 */
public class UserRetrXxExperimentRandShortCBugMain {
	public static void main(String[] args) {
		boolean dataTransferConfirmationBug = true;
		boolean pasvResponseReadingBug = false;
		UserRetrXxShortExperiment userExperiment = new UserRetrXxShortExperiment(
				dataTransferConfirmationBug, pasvResponseReadingBug, CClient.NET_CODE_RAND, false);
		userExperiment.start();
	}
}
