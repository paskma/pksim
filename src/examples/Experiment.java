package examples;

import pksim.core.Simulation;

public abstract class Experiment {
	protected Simulation simulation;
	public Simulation getSimulation() {
		return simulation;
	}

	public abstract void start();
}
