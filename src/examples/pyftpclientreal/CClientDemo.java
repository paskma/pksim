package examples.pyftpclientreal;

import pyftpclient_layer.CClient;
import pyftpclient_layer.CFileStream;
import pyftpclient_layer.CFtpFile;

import java.io.ByteArrayOutputStream;

public class CClientDemo {
	static void p(Object o) {
		System.out.println(o);
	}

	/**
	 * Simple happy-day scenario with built-in server.
	 */
	public static void demoTestNetwork() {
		p("TestNetwork demo");
		CClient client = new CClient(CClient.NET_TEST);
		client.connect("ignored", 21);
		client.login("anonymous", "osgiftp@kiv.zcu.cz");

		CFtpFile lst[];
		lst = client.listFiles();

		for (CFtpFile i : lst) {
			System.out.println(i);
		}

		byte[] contentXX = client.retrieveFile("xx");
		p("XX:'" +  (new String(contentXX)) + "'");
		client.logout();
	}

	/**
	 * Demonstrates an interaction with real FTP server.
	 */
	public static void demoWild() {
		String host;
		host = "ftp.zcu.cz";
		host = "ftp.gnu.org";
		CClient client = new CClient();
		client.connect(host, 21);
		client.login("anonymous", "osgiftp@kiv.zcu.cz");

		CFtpFile lst[];
		lst = client.listFiles();

		for (CFtpFile i : lst) {
			System.out.println(i);
		}

		client.changeWorkingDirectory("pub");

		lst = client.listFiles();

		for (CFtpFile i : lst) {
			System.out.println(i);
		}

		byte[] welcome = client.retrieveFile("welcome.msg");
		if (welcome != null)
			System.out.println(new String(welcome));

		CFileStream stream = client.retrieveFileStream("welcome.msg");
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int c;
		while ((c = stream.read()) != -1) {
			buffer.write((byte)c);
			break;
		}
		stream.close();
		System.out.println("From stream:");
		System.out.println(new String(buffer.toByteArray()));


		System.out.println("Still healthy?");
		welcome = client.retrieveFile("welcome.msg");

		System.out.println("Desperate delete");
		client.deleteFile("mythical_file.docxxx");

		client.logout();
		System.out.println("Done.");
	}

	/**
	 * Demonstration of client working agains a built-in server that fails
	 * to confirm a data transfer (however it does not break the protocol).
	 *
	 * If the client contains a bug the data transfer seems to go well (which is wrong)
	 * and the client stucks in wrong state. An exception is raised upon next operation.
	 *
	 * If the client is bug-free then it figures out that the data transfer failed
	 * and returns null. State transitions are ok.
	 */
	public static void demoFail(boolean clientDataConfirmationBug) {
		if (clientDataConfirmationBug)
			p("C:Server fails, client raises exception due to a bug");
		else
			p("C:Server fails, client handles that gracefully");

		CClient client = new CClient(CClient.NET_TEST_CONFIRMATION_FAIL);
		client.setDataTransferConfirmationBug(clientDataConfirmationBug);
		client.connect("ignored", 21);
		client.login("anonymous", "osgiftp@kiv.zcu.cz");

		byte[] f = client.retrieveFile("xx");
		if (f != null)
			p("C:File is:\n"+ new String(f));
		else
			p("C:File transfer failed.");

		p("C:##Second shot...");

		f = client.retrieveFile("xx");
		if (f != null)
			p("C:File(2) is:\n"+ new String(f));
		else
			p("C:File(2) transfer failed.");

		client.logout();

	}
}
