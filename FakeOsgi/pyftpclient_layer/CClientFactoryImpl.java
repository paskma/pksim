package pyftpclient_layer;

public class CClientFactoryImpl implements CClientFactory {
	protected final int networkType;
	protected boolean dataTransferConfirmationBug = false;
	protected boolean pasvResponseReadingBug = false;


	public CClientFactoryImpl(int networkType) {
		this.networkType = networkType;
	}


	@Override
	public ICClient createCClient() {
		CClient result = new CClient(networkType);
		result.setDataTransferConfirmationBug(dataTransferConfirmationBug);
		result.setPasvResponseReadingBug(pasvResponseReadingBug);
		return result;
	}

	public void setDataTransferConfirmationBug(boolean dataTransferConfirmationBug) {
		this.dataTransferConfirmationBug = dataTransferConfirmationBug;
	}

	public void setPasvResponseReadingBug(boolean pasvResponseReadingBug) {
		this.pasvResponseReadingBug = pasvResponseReadingBug;
	}
}
