package filemanager.utils;

/**
 * Abstraction of a file (plain or directory).
 */
public interface IFSNode {

	/**
	 * This returns total content of the file.
	 * Can not handle huge files.
	 * TODO: provide getStream alternative
	 */
	byte[] getContent();
	
	/**
	 * Returns directory listing if the current node is a directory, null otherwise.
	 */
	IFSNode[] getDirectoryListing();
    IFSNode getSubNode(String filename, boolean directory);
	
	boolean isDirectory();
	
	/**
	 * @return Short name (relative)
	 */
	String getName();
	
	/**
	 * @return Fully qualified path
	 * TODO: URL?
	 */
	String getAbsolutePath();
    /* hack */
    void _reset();
}
