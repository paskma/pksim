package filemanager.utils;


public interface IFileAccess {
	public IFSNode getFSNode(String pathname);
	public boolean deleteFSNode(String pathname, boolean deleteSubdir);
	public boolean copyFSNode(String sourceName, String destName);
	public boolean renameFSNode(String oldName, String newName);
}
